import pyb
from pyb import Pin, Timer
'''
音调:1 Do 2 Re 3 Mi 4 Fa 5 So 6 La 7 Si
频率: 262  294  330  349  392  440  494

p = Pin('Y3')  
tim = Timer(10, freq=500)
ch = tim.channel(1, Timer.PWM, pin=p)
ch.pulse_width_percent(50)
'''

class Beep:
    Phonogram = {'Do': 262, 'Re': 294, 'Mi': 330,
                 'Fa': 349, 'So': 392, 'La': 440, 'Si': 494, 'Start': 1350}

    def __init__(self, timeNum, beepin, ch):
        self.p = Pin(beepin)  # Y3 has TIM4, CH3
        self.timer = Timer(timeNum, freq=500)
        self.ch = self.timer.channel(ch, Timer.PWM, pin=self.p)
        self.ch.pulse_width_percent(50)
        self.off()

    def on(self):
        self.ch.pulse_width_percent(50)

    def play(self, note, duration):
        freq = self.Phonogram.get(note, None)
        if freq is None:
            raise ValueError('Invalid note: %s' % note)
        pulse_width = int(self.timer.source_freq() / (2 * freq))
        self.ch.pulse_width(pulse_width)
        self.ch.pulse_width_percent(50)
        self.timer.freq(freq)
        pyb.delay(duration)
        print("beep!")
        self.off()

    def off(self):
        self.ch.pulse_width_percent(0)
