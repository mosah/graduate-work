import pyb
from pyb import Pin

class LOCK:
    def __init__(self):
        self.StaPin1 = Pin('X5', Pin.OUT_PP)
        self.StaPin2 = Pin('X6', Pin.IN, Pin.PULL_DOWN)
        self.ControlPin = Pin('Y11', Pin.OUT_PP)
        self.StaPin1.high()
        self.ControlPin.high()
        self.state = self.StaPin2.value()

    def unlock(self):
        self.ControlPin.low()
        pyb.delay(1500)
        print("Unlock!")
        self.ControlPin.high()

    def getState(self):
        self.state = self.StaPin2.value()
        return self.state
