import pyb
from pyb import RTC
from ssd1306 import SSD1306

display = SSD1306(1, pinout={'sda': 'PB7', 'scl': 'PB6'}, height=64, external_vcc=False)

def displayinit():
    display.poweron()  # 开启 OLED 电源
    display.init_display()  # 初始化显示

def clear():
    display.clear()


def homePage(tem, hum, lockSta):
    display.clear()
    display.draw_text(1, 1, 'Pess left/right key')  # 显示位置，显示内容，写入显存
    display.draw_text(1, 13, 'to scan/set system')  # 显示位置，显示内容
    display.draw_text(1, 25, 'Lock state:' + lockSta)  # 显示位置，显示内容
    display.draw_text(1, 37, hum)  # 显示位置，显示内容
    display.draw_text(1, 49, tem)  # 显示位置，显示内
    display.display()  # 显示显存


def timePage(dmy,hms):
    display.clear()
    display.draw_text(1, 1, 'System Time')  # 显示位置，显示内容，写入显存
    display.draw_text(1, 25, dmy)  # 显示位置，显示内容
    display.draw_text(1, 37, hms)  # 显示位置，显示内容
    display.display()  # 显示显存

def energyPage(lineOnePower, lineTwoPower):
    display.clear()
    display.draw_text(1, 1, 'energyPage')  # 显示位置，显示内容，写入显存
    display.draw_text(1, 25, 'lineOnePower:' + lineOnePower)  # 显示位置，显示内容
    display.draw_text(1, 49, 'lineTwoPower:' + lineTwoPower)  # 显示位置，显示内容
    display.display()  # 显示显存


def setRelayPage(x, y):
    display.clear()
    display.draw_text(1, 1, 'setRelayPage')  # 显示位置，显示内容，写入显存
    display.draw_text(x, y, 'o')  # 显示位置，显示内容
    display.draw_text(18, 25, 'relay_one: on/off')  # 显示位置，显示内容
    display.draw_text(18, 49, 'relay_two: on/off')  # 显示位置，显示内容
    display.display()  # 显示显存


def setResTime_1_Page(x, y, remainTime):
    display.clear()
    display.draw_text(1, 1, 'setResTime_1_Page')  # 显示位置，显示内容，写入显存
    display.draw_text(1, 13, 'restime:')  # 显示位置，显示内容
    display.draw_text(15, 25, '2023-1-1 12:00:00')  # 显示位置，显示内容
    display.draw_text(x, y, '_')  # 显示位置，显示内容
    display.draw_text(1, 49, 'remain time:' + remainTime + 's')
    display.display()  # 显示显存


def setResTime_2_Page(x, y, remainTime):
    display.clear()
    display.draw_text(1, 1, 'setResTime_2_Page')  # 显示位置，显示内容，写入显存
    display.draw_text(1, 13, 'restime:')  # 显示位置，显示内容
    display.draw_text(15, 25, '2023-1-1 12:00:00')  # 显示位置，显示内容
    display.draw_text(x, y, '_')  # 显示位置，显示内容
    display.draw_text(1, 49, 'remain time:' + remainTime + 's')
    display.display()  # 显示显存
