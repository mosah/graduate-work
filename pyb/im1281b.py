# im1281b 简易数据读取
import binascii
from pyb import UART

# register definitions
Default_Baud = const(4800)
Default_addr = const(0x01)
readAllDataCode = b'\x01\x03\x00\x48\x00\x08\xC4\x1A'


class IM1281B:
    def __init__(self, uartcom, Default_Baud):
        self.im1281buart = UART(uartcom, Default_Baud)
        self.im1281buart.init(Default_Baud, bits=8, parity=None, stop=1)
        # 寄存器参数
        self.voltage = 0
        self.current = 0
        self.activePower = 0
        self.activeEnergy = 0
        self.powerFactor = 0
        self.CO2Emissions = 0
        self.temperature = 0
        self.frequency = 0
        # self.getdata         = " "
        # self.addrAndBaud     = 0
        self.data = None
        print("im1281b init ok")

    def data2params(self):
        params = [6, 14, 22, 30, 38, 46, 54, 62]
        factors = [0.0001, 0.0001, 0.0001, 0.0001, 0.001, 0.0001, 0.01, 0.01]
        for i in range(len(params)):
            setattr(self, ['voltage', 'current', 'activePower', 'activeEnergy', 'powerFactor', 'CO2Emissions',
                    'temperature', 'frequency'][i], factors[i] * hex2dec(self.data[params[i]: params[i]+8]))

    def readAllData(self):
        self.im1281buart.write(readAllDataCode)
        self.data = self.im1281buart.read()
        if isinstance(self.data, bytes):
            self.data = binascii.hexlify(self.data).decode()  # 转换成str
            if (calc_crc(self.data) == "0000"):
                self.data2params()
                return self.data.upper()
        else:
            pass

# crc校验


def calc_crc(string):
    if len(string) % 2 != 0:
        string = '0' + string
    data = bytearray()
    i = 0
    while i < len(string):
        byte = string[i:i+2]
        data.append(int(byte, 16))
        i += 2
    crc = 0xFFFF
    for pos in data:
        crc ^= pos
        for i in range(8):
            if (crc & 1) != 0:
                crc >>= 1
                crc ^= 0xA001
            else:
                crc >>= 1
    # 修改格式化字符串将低8位放在前面
    hex_crc = "{:04X}".format(crc)[2:] + "{:04X}".format(crc)[:2]
    return hex_crc  # 返回CRC校验码

# hex’s str to dec int
def hex2dec(data):
	data = '0x' + data
	return int(data, 16)


if __name__ == '__main__':  # 程序入口
    pass
