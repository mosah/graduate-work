import utime
import time
import pyb


class Relay:
    def __init__(self, controlpin):
        self.controlpin = controlpin
        self.state = False
        self.disconnect()

    def disconnect(self):
        self.controlpin.high()

    def resState(self):
        print("controlpin.value:", self.controlpin.value())
        if self.controlpin.value() == 0:
            self.controlpin.high()
        elif self.controlpin.value() == 1:
            self.controlpin.low()

    def countdownResState(self, resUnixTime):  # countdown：倒计时时间戳
        epochTime = 946656000
        sysUnixTime = utime.time() + epochTime
        print("resUnixTime:", resUnixTime)
        print("sysUnixTime:", sysUnixTime)
        countdownSec = resUnixTime - sysUnixTime
        print("countdownSec:", countdownSec)
        if countdownSec > 0:
            self.countdown(countdownSec)

    def countdown(self, seconds):
        def countdown_callback(timer):
            # 倒计时结束后调用回调函数
            timer.callback(None)  # 关闭 Timer
            print("timer callback")
            timer.deinit()
            self.resState()
        # 创建 Timer 对象
        timer = pyb.Timer(2)
        # 计算定时器的频率
        # freq = 1
        # 设置定时时间（单位：毫秒）
        time_ms = seconds * 1000
        # 设置重载值
        timer.init(period=time_ms, mode=timer.DOWN)
        # 设置回调函数
        timer.callback(countdown_callback)
