import time
from pyb import Pin
from relay import Relay
from lock import LOCK

# relay1ContrPin = Pin('X11', Pin.OUT_PP)
# relay2ContrPin = Pin('X12', Pin.OUT_PP)
# lockContrPin = pyb.Pin('Y11', pyb.Pin.OUT_PP)

# relay_1 = Relay(relay1ContrPin)
# relay_2 = Relay(relay2ContrPin)

class otherdata:

    def __init__(self, aht20):
        self.aht20 = aht20
        self.relay1 = Pin('X11', Pin.OUT_PP)
        self.relay2 = Pin('X12', Pin.OUT_PP)
        # boxEvn = humval + temval + relay1Sta + relay2Sta + bswSta + bsw
        self.hum = ""
        self.humval = ""
        self.tem = ""
        self.temval = ""
        self.box_switch = 0  #0表示关，1表示开
        self.relay1_sta = 0
        self.relay2_sta = 0
        self.lockSte = ""
        self.relay1_reverse_time = ""
        self.relay2_reverse_time = ""
        self.sys_time = ""
        self.boxEvn = ""
        self.lock = LOCK()

    def update(self):#只负责读取，不负责修改
        self.box_switch = self.lock.getState()
        self.temval = "{:.2f}".format(self.aht20.temperature)
        self.humval = "{:.2f}".format(self.aht20.relative_humidity)
        self.hum = "Humidity: " + self.humval + "%"
        self.tem = "Temperature: " + self.temval + "C"
        self.relay1_sta = self.relay1.value()
        self.relay2_sta = self.relay2.value()
        self.boxEvn = (self.temval + str(self.humval) + \
                       str(self.box_switch) + str(self.relay1_sta) + str(self.relay2_sta)).replace('.', '', 2)
        if self.box_switch == 1:
            self.lockSte = "off"
        elif self.box_switch == 0:
            self.lockSte = "on"



# if __name__ == "__main__":
#     aht20 = ...  # 创建AHT20传感器对象
#     data = OtherData(aht20)
#     data.start(aht20)


