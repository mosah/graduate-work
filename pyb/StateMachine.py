import pyb
import utime
import systime
import userInterface
from pyb import Pin
from relay import Relay

relay1ContrPin = pyb.Pin('X11', pyb.Pin.OUT_PP)
relay2ContrPin = pyb.Pin('X12', pyb.Pin.OUT_PP)
# lockContrPin = pyb.Pin('Y11', pyb.Pin.OUT_PP)

relay_1 = Relay(relay1ContrPin)
relay_2 = Relay(relay2ContrPin)

# 定义状态
STATE_INIT = 0
STATE_IDLE = 1
STATE_TIME = 2
STATE_ENERGY = 3
STATE_SETRELAY = 4
STATE_SETRESTIME_1 = 5
STATE_SETRESTIME_2 = 6
STATE_RUNNING = 7
STATE_ERROR = 8

# 定义事件
EVENT_START = 0
EVENT_STOP = 1
EVENT_ERROR = 2
EVENT_UP = 3
EVENT_DOWN = 4
EVENT_RIGHT = 5
EVENT_LEFT = 6
EVENT_CENTER = 7

# 定义状态转移表
state_table = {
    # 当前状态
    STATE_INIT: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_SETRESTIME_2,
        EVENT_RIGHT: STATE_ENERGY,
        EVENT_CENTER: STATE_INIT,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_ENERGY: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_INIT,
        EVENT_RIGHT: STATE_SETRELAY,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_SETRELAY: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_ENERGY,
        EVENT_RIGHT: STATE_TIME,
        EVENT_DOWN:STATE_SETRELAY,
        EVENT_UP: STATE_SETRELAY,
        EVENT_CENTER: STATE_SETRELAY,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_TIME: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_SETRELAY,
        EVENT_RIGHT: STATE_SETRESTIME_1,
        EVENT_CENTER: STATE_TIME,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_SETRESTIME_1: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_TIME,
        EVENT_RIGHT: STATE_SETRESTIME_2,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_SETRESTIME_2: {
        EVENT_START: STATE_RUNNING,  # 事件对应转移状态
        EVENT_LEFT: STATE_SETRESTIME_1,
        EVENT_RIGHT: STATE_INIT,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_IDLE: {
        EVENT_START: STATE_RUNNING
    },
    STATE_RUNNING: {
        EVENT_STOP: STATE_IDLE,
        EVENT_ERROR: STATE_ERROR
    },
    STATE_ERROR: {
        EVENT_START: STATE_INIT
    }
}

p_in = Pin('X7', Pin.IN, Pin.PULL_UP)
epochTime = 946656000

# 定义状态机类
class StateMachine:
    def __init__(self, dtu, wdt):
        self.wdt = wdt
        self.dtu = dtu
        self.otherdatas = dtu.otherData
        self.im1281b_1 = dtu.im1281b_1
        self.im1281b_2 = dtu.im1281b_2
        self.date = pyb.RTC().datetime()
        self.adcval = pyb.ADC(Pin('X8')).read()
        self.dmy, self.hms = systime.rtc_to_datestr(self.date)
        userInterface.displayinit()
        self.current_state = STATE_INIT
        self.count = 0
        self.lastEvent = 0
        self.dtu.setLocalTime(pyb.RTC())
        self.handle_state(self.current_state)

    # 触发事件
    def trigger_event(self, event):
        self.lastEvent = event
        if self.current_state is None:
            self.current_state = STATE_INIT
        if event in state_table[self.current_state]:
            self.current_state = state_table[self.current_state][event]
            self.handle_state(self.current_state)

    # 处理状态
    def handle_state(self, state):
        if state == STATE_INIT:
            print("Init State Home")
            if self.lastEvent == EVENT_CENTER:
                print("last event is EVENT_CENTER")
                self.otherdatas.lock.unlock()
            while True:
                self.wdt.feed()
                userInterface.homePage(self.otherdatas.tem, self.otherdatas.hum, self.otherdatas.lockSte)
                if p_in.value() == 0:
                    break


        elif state == STATE_ENERGY:
            print("STATE_ENERGY")
            while True:
                self.wdt.feed()
                userInterface.energyPage(str(self.im1281b_1.activePower), str(self.im1281b_2.activePower))
                # pyb.delay(1000)
                if p_in.value() == 0:
                    break
                # self.trigger_event(self.current_state)  # 处理新状态

        elif state == STATE_SETRELAY:
            print("STATE_SETRELAY")
            if self.lastEvent == EVENT_DOWN or self.lastEvent == EVENT_UP:
                self.count = self.count + 1
            if self.count%2 :
                y = 25
            else:
                y = 49
            if self.lastEvent == EVENT_CENTER:
                print("last event is EVENT_CENTER")
                self.lastEvent = EVENT_START
                if y == 25:
                    relay_1.resState()
                if y == 49 :
                    relay_2.resState()
            while True:
                self.wdt.feed()
                userInterface.setRelayPage(1, y)
                pyb.delay(250)
                if p_in.value() == 0:
                    break

        elif state == STATE_TIME:
            print("STATE_TIME")
            self.count = 0
            # self.dtu.setLocalTime(pyb.RTC())
            while True:
                self.wdt.feed()
                self.date = pyb.RTC().datetime()
                self.dmy, self.hms = systime.rtc_to_datestr(self.date)
                userInterface.timePage(self.dmy, self.hms)
                if p_in.value() == 0 :
                    break

        elif state == STATE_SETRESTIME_1:
            print("STATE_SETRESTIME_1")
            while True:
                self.wdt.feed()
                sysUnixTime = utime.time() + epochTime
                if self.dtu.countdown1 > sysUnixTime:
                    remianTime = self.dtu.countdown1 - sysUnixTime
                elif self.dtu.countdown1 <= sysUnixTime:
                    remianTime = 0
                userInterface.setResTime_1_Page(15, 37, str(remianTime))
                if p_in.value() == 0:
                    break

        elif state == STATE_SETRESTIME_2:
            print("STATE_SETRESTIME_2")
            while True:
                self.wdt.feed()
                sysUnixTime = utime.time() + epochTime
                if self.dtu.countdown2 > sysUnixTime:
                    remianTime = self.dtu.countdown2 - sysUnixTime
                elif self.dtu.countdown2 <= sysUnixTime:
                    remianTime = 0
                userInterface.setResTime_2_Page(15, 37, str(remianTime))
                if p_in.value() == 0:
                    break

        elif state == STATE_IDLE:
            print("Idle state")
            # 空闲状态代码
        elif state == STATE_RUNNING:
            print("Running state")
            # 运行状态代码
        elif state == STATE_ERROR:
            while True:
                print("Error state")
                print("wait watch dog restart")

# 主程序
if __name__ == "__main__":
    # 初始化状态机
    sm = StateMachine()

    # 触发事件
    sm.trigger_event(EVENT_START)
    sm.trigger_event(EVENT_STOP)
    sm.trigger_event(EVENT_ERROR)
    sm.trigger_event(EVENT_START)
