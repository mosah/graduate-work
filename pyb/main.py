import pyb
import _thread
import ahtx0
from correspondWithDTU import corrDTU
from pyb import Pin, ExtInt, Timer
from machine import I2C, WDT
from beep import Beep
from relay import Relay
from im1281b import IM1281B
from otherData import otherdata
from StateMachine import StateMachine

sw = ""
swCounter = 0
lastSwCounter = 0

# 定义按键事件
EVENT_UP = 3
EVENT_DOWN = 4
EVENT_RIGHT = 5
EVENT_LEFT = 6
EVENT_CENTER = 7

# 初始化
relay_1 = Relay(Pin('X11', Pin.OUT_PP))
relay_2 = Relay(Pin('X12', Pin.OUT_PP))

i2c = I2C(scl='Y9', sda='Y10')
buzzer = Beep(10, 'Y3', 1)
adc = pyb.ADC(Pin('X8'))
sensor = ahtx0.AHT20(i2c)
im1281b_1 = IM1281B(4, 4800)
im1281b_2 = IM1281B(6, 4800)
otherdatas = otherdata(sensor)
dtu = corrDTU(im1281b_1, im1281b_2, otherdatas)

# （年、月、日、工作日、小时、分钟、秒、亚秒）
pyb.RTC().datetime((2023, 5, 1, 4, 10, 0, 0, 0))

# 看门狗
wdt = WDT(timeout=5000)
# 中断函数
def callback(line):
	global sw, swCounter, lastSwCounter
	swCounter = 1
	lastSwCounter = 0
	# swCounter = swCounter + 1
	adcval = adc.read()
	# print(adcval)
	if adcval > 3400:
		sw = "down"
	elif adcval > 2400:
		sw = "center"
	elif adcval > 2000:
		sw = "up"
	elif adcval > 900:
		sw = "right"
	elif adcval >= 0:
		sw = "left"
sw = ExtInt(Pin('X7'), ExtInt.IRQ_FALLING, Pin.PULL_NONE, callback)

# 线程函数
def getData(sec):  # 线程 A 任务
	pyb.delay(sec)  # 线程 A 任务延时
	while True:  # 循环
		otherdatas.update()
		lineOne = im1281b_1.readAllData()
		lineTwo = im1281b_2.readAllData()
		# print("lineone:", lineOne)
		# print("linotwo:", lineTwo)
		pyb.LED(4).toggle()  # 闪灯
		pyb.delay(500)  # 延时

def showAndControl(sec):
	global sw, swCounter, lastSwCounter
	enStateMachine = 8
	while True:
		wdt.feed()
		if enStateMachine == 1:
			print("statemachine start")
			buzzer.play('Start', 500)
			sm = StateMachine(dtu, wdt)
		if enStateMachine >= 1:
			enStateMachine = enStateMachine - 1
		if ((swCounter - lastSwCounter) == 1) :
			lastSwCounter = swCounter
			pyb.LED(3).toggle()  # 闪灯
			if sw == "right":
				buzzer.play('Do', 100)
				sm.trigger_event(EVENT_RIGHT)
			elif sw == "left":
				buzzer.play('Re', 100)
				sm.trigger_event(EVENT_LEFT)
			elif sw == "up":
				buzzer.play('Mi', 100)
				sm.trigger_event(EVENT_UP)
			elif sw == "down":
				buzzer.play('Fa', 100)
				sm.trigger_event(EVENT_DOWN)
			elif sw == "center":
				buzzer.play('So', 100)
				sm.trigger_event(EVENT_CENTER)
		pyb.delay(500)

def corrspondDTU(sec):
	count = 200
	while True:
		dtu.controlLocalDev(relay_1, relay_2)
		if count == 0:
			dtu.pushData(7000)
			count = 200
		else :
			count = count - 1
		if count%2:
			pyb.LED(2).toggle()  # 闪灯
		pyb.delay(50)  # 延时

if __name__ == '__main__':  # 程序入口
	_thread.start_new_thread(getData, (1,))  # 开启线程 A
	_thread.start_new_thread(showAndControl, (1,))
	_thread.start_new_thread(corrspondDTU, (1,))
