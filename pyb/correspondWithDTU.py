import binascii
import pyb

class corrDTU:
    def __init__(self, im1281b_1, im1281b_2, otherDatas):
        self.im1281b_1 = im1281b_1
        self.im1281b_2 = im1281b_2
        self.otherData = otherDatas
        self.uart = pyb.UART(2, 115200)
        self.cloudControlCommand = ""
        self.pushEn = False
        self.getContrEn = False
        self.uart.init(115200, bits=8, parity=None, stop=1, read_buf_len=128)
        self.countdown1 = 0
        self.countdown2 = 0

    def setLocalTime(self, RTC):
        self.pushEn = False
        self.getContrEn = False
        while True:
            self.uart.write("config,get,nettime\r\n")
            time = self.uart.read()
            pyb.delay(1000)
            if isinstance(time, bytes):
                time = time.decode()
                print("time")
                if time[2:20] == "config,nettime,ok,":
                    split_data = time.split(",")
                    year, month, day, hour, minute, second, milsecond = map(int, split_data[3:10])
                    RTC.datetime((year, month, day, 0, hour, minute, second, milsecond))
                    self.pushEn = True
                    self.getContrEn = True
                    print("setTimeOk")
                    return 0
            self.pushEn = False

    def pushData(self, sec):
        # pyb.delay(sec)
        if self.pushEn:
            if (self.im1281b_1.data != None):
                im1281b1Data = "111" + self.im1281b_1.data
                self.uart.write(im1281b1Data)
                print("pushData1-ok")
                pyb.delay(500)
            if (self.im1281b_2.data != None):
                im1281b2Data = "000" + self.im1281b_2.data
                self.uart.write(im1281b2Data)
                pyb.delay(500)
                print("pushData2-ok")
            # boxEvn = humval + temval + relay1Sta + relay2Sta + bswSta + bsw
            # relayOffTime
            if len(self.otherData.boxEvn) == 11 :
                self.uart.write("001" + self.otherData.boxEvn)
                print("pushData3-ok")
            pyb.delay(500)
            # self.uart.write("101" + self.otherData.relay1_reverse_time)
            # pyb.delay(1000)
            # self.uart.write("010" + self.otherData.relay2_reverse_time)
        elif self.pushEn == False :
            print("pushEn_False")
    
    def controlLocalDev(self, relay_1, relay_2):
        if self.getContrEn :
            self.pushEn = False
            self.cloudControlCommand = self.uart.read()
            if isinstance(self.cloudControlCommand, bytes):
                self.cloudControlCommand = self.cloudControlCommand.decode()  # 转换成str
                print(self.cloudControlCommand)
                if self.cloudControlCommand[0:4] == "CRLO" :
                    relay_1.resState()
                elif self.cloudControlCommand[0:4] == "CRLT":
                    relay_2.resState()
                # CountDown_CRLO
                elif self.cloudControlCommand[0:14] == "CountDown_CRLO":
                    self.countdown1 = int(self.cloudControlCommand[18:])
                    print(self.countdown1)
                    relay_1.countdownResState(self.countdown1)
                elif self.cloudControlCommand[0:14] == "CountDown_CRLT":
                    self.countdown2 = int(self.cloudControlCommand[18:])
                    print(self.countdown2)
                    relay_2.countdownResState(self.countdown2)
            self.pushEn = True

