import utime
import time

def unix_timestamp_to_datetime(timestamp):
    dt_object = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(timestamp))
    return dt_object


def to_unix_timestamp(dt_tuple):
    # 将 UTC+8 的小时数转换成 UTC 的小时数
    dt_utc_tuple = dt_tuple[:3] + (dt_tuple[3] - 8,) + dt_tuple[4:]
    # 获取 UTC 时间戳（单位为秒）
    timestamp_utc = time.mktime(dt_utc_tuple)
    # 计算对应的本地时间戳（单位为秒）
    local_timezone = time.mktime(time.localtime()) - time.mktime(time.gmtime())
    timestamp_local = timestamp_utc + local_timezone
    return int(timestamp_local)  # 输出：1682920800

# （年、月、日、工作日、小时、分钟、秒、亚秒）


def rtc_to_datestr(tup):
    dmy = ""    # 年月日
    hms = ""    # 时分秒
    for i in range(len(tup)):
        if i < 3:
            if i == len(tup) - 1:
                dmy += str(tup[i])
            else:
                dmy += str(tup[i]) + "/"
        elif i > 3 :
            if i == len(tup) - 1:
                hms += str(tup[i])
            else:
                hms += str(tup[i]) + ":"
    return dmy, hms


if __name__ == "__main__":
    timestamp = 1622749200  # Unix时间戳，2021年6月4日20:00:00
    datetime_str = unix_timestamp_to_datetime(timestamp)
    print(datetime_str)  # 输出：'2021-06-04 20:00:00'
