// 引入包
var iot = require('../../dist/alibabacloud-iot-device-sdk.min.js');
var dateTimePicker = require('../../utils/dateTimePicker.js');

const sdk_device = {
  "productKey": "i3vaquCcjoB",
  "deviceName": "wchat_smartweb", 
  "deviceSecret": "a52f58f179065152a67c7d00fe7e5f47",
  "protocol": 'wxs://',
} 
var subTopic = '/i3vaquCcjoB/wchat_smartweb/user/getPhyModelPar';
var pusTopic = '/i3vaquCcjoB/wchat_smartweb/user/postControl';
var ntpTopic = '/ext/ntp/i3vaquCcjoB/wchat_smartweb/request'
var testmag = 'CRLOsta:0,CRLTsta:1'
//连接云平台
const devicex = iot.device(sdk_device);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    distributorBox:{
        tem : 0, hum : 0, boxSwith : false, boxName : "配电箱",
        LineOne : {
            voltage         : 0,
            current         : 0,
            activePower     : 0,
            activeEnergy    : 0,
            powerFactor     : 0,
            CO2Emissions    : 0,
            mcutem          : 0,
            frequency       : 0,
            Relay           : false,
            EnResRelay      : false,
        },
        LineTwo : {
            voltage         : 0,
            current         : 0,
            activePower     : 0,
            activeEnergy    : 0,
            powerFactor     : 0,
            CO2Emissions    : 0,
            mcutem          : 0,
            frequency       : 0,
            Relay           : true,
            EnResRelay      : false,
        }
    },boxdata: {},
    date: '2018-10-01',
    time: '12:00',
    dateTimeArray: null,
    dateTime: null,
    dateTimeArray1: null,
    dateTime1: null,
    startYear: 2000,
    endYear: 2050
  },
  //定时启动
  setTimeOpen(e){
    console.log('ok_setTimeOpen');
  },
    //控制继电器
    setRelay1(e){
        // 向 testtopic 主题发送一条 QoS 为 0 的测试消息
        if (this.data.distributorBox.LineOne.Relay == true) {
            devicex.publish(pusTopic, 'CRLOsta_false', { qos: 0, retain: false }, function (error) {
                if (error) {
                    console.log(error)
                } else {
                console.log('Published','set_relay_1_false')
                }
            });
            this.setData({
                'distributorBox.LineOne.Relay': false
            });
        }
        else {
            devicex.publish(pusTopic, 'CRLOsta_true', { qos: 0, retain: false }, function (error) {
                if (error) {
                    console.log(error)
                } else {
                console.log('Published','set_relay_1_true')
                }
            });
            this.setData({
                'distributorBox.LineOne.Relay': true
            });
        }
    },
    setRelay2(e){
        let setMessage = 'CRLTsta_false'
        if (this.data.distributorBox.LineTwo.Relay == true) {
            devicex.publish(pusTopic, setMessage, { qos: 0, retain: false }, function (error) {
                if (error) {
                    console.log(error)
                } else {
                console.log('Published','set_relay_2_false')
                }
            });
            this.setData({
                'distributorBox.LineTwo.Relay': false
            });
        }
        else {
            devicex.publish(pusTopic, 'CRLTsta_true', { qos: 0, retain: false }, function (error) {
                if (error) {
                    console.log(error)
                } else {
                console.log('Published','set_relay_2_true')
                }
            });
            this.setData({
                'distributorBox.LineTwo.Relay': true
            });
        }
    },
    setRelay1CountDownRes(e){
        var countdown = this.getUnixTime()
        var countdownStr = 'CountDown_CRLOsta_' + countdown.toString()
        devicex.publish(pusTopic, countdownStr, { qos: 0, retain: false }, function (error) {
            if (error) {
                console.log(error)
            } else {
            console.log('Published',countdownStr)
            }
        });
    },
    setRelay2CountDownRes(e){
        var countdown = this.getUnixTime()
        var countdownStr = 'CountDown_CRLTsta_' + countdown.toString()
        devicex.publish(pusTopic, countdownStr, { qos: 0, retain: false }, function (error) {
            if (error) {
                console.log(error)
            } else {
            console.log('Published',countdownStr)
            }
        });
    },
    getUnixTime(e){
        var arr = this.data.dateTime; // 创建一个日期对象
        const date = new Date();
        date.setFullYear(2000+arr[0]);
        date.setMonth(arr[1]);
        date.setDate(arr[2]+1);
        date.setHours(arr[3]);
        date.setMinutes(arr[4]);
        date.setSeconds(arr[5]);
        date.setMilliseconds(0);
        console.log(date);
        var unixTimestamp = Math.floor(date.getTime() / 1000); // 获取Unix时间戳
        console.log(typeof unixTimestamp); // 输出Unix时间戳
        return unixTimestamp;
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取完整的年月日 时分秒，以及默认显示的数组
    var obj = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    var obj1 = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    // 精确到分的处理，将数组的秒去掉
    var lastArray = obj1.dateTimeArray.pop();
    var lastTime = obj1.dateTime.pop();

    this.setData({
      dateTime: obj.dateTime,
      dateTimeArray: obj.dateTimeArray,
      dateTimeArray1: obj1.dateTimeArray,
      dateTime1: obj1.dateTime
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    // 当连接成功进入回调
    
    devicex.on('connect', () => {
        console.log('>>>>>device connect succeed');
    });

    devicex.on('message', function (topic, message) {
        // message is Buffer
        let msg = message.toString();
        const obj = JSON.parse(msg);
        const { time, ...newObj } = obj; // 使用对象解构赋值忽略时间戳属性
        console.log(this.data.dateTime)
        console.log(obj);
        this.data.boxdata = obj;
        this.transdata(obj)
        this.setData({
            distributorBox: this.data.distributorBox
          })
    }.bind(this));
      
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  transdata(e){
    const { distributorBox } = this.data
    if (e.BoxSwitch?.value !== undefined) distributorBox.boxSwith = e.BoxSwitch.value.toString() || "0"
    if (e.Temperature?.value !== undefined) {
        distributorBox.tem = e.Temperature.value.toString() || "0"
      }
    if (e.Humidity?.value !== undefined) distributorBox.hum = e.Humidity.value.toString() || "0"
  
    const { LineOne, LineTwo } = distributorBox
    if (e.ActiveEnergy_1?.value !== undefined) LineOne.activeEnergy = e.ActiveEnergy_1.value.toString() || "0"
    if (e.ActivePower_1?.value !== undefined) LineOne.activePower = e.ActivePower_1.value.toString() || "0"
    if (e.LOA?.value !== undefined) LineOne.current = e.LOA.value.toString() || "0"
    if (e.EMStem_1?.value !== undefined) LineOne.mcutem = e.EMStem_1.value.toString() || "0"
    if (e.PowerFactor_1?.value !== undefined) LineOne.powerFactor = e.PowerFactor_1.value.toString() || "0"
    if (e.LOV?.value !== undefined) LineOne.voltage = e.LOV.value.toString() || "0"
    if (e.CO2Emissions_1?.value !== undefined) LineOne.CO2Emissions = e.CO2Emissions_1.value.toString() || "0"
    if (e.Frequency_1?.value !== undefined) LineOne.frequency = e.Frequency_1.value.toString() || "0"
    if (e.CRLOsta?.value == 0) LineOne.Relay = false
    else if(e.CRLOsta?.value == 1) LineOne.Relay = true
  
    if (e.ActiveEnergy_2?.value !== undefined) LineTwo.activeEnergy = e.ActiveEnergy_2.value.toString() || "0"
    if (e.ActivePower_2?.value !== undefined) LineTwo.activePower = e.ActivePower_2.value.toString() || "0"
    if (e.LTA?.value !== undefined) LineTwo.current = e.LTA.value.toString() || "0"
    if (e.EMStem_2?.value !== undefined) LineTwo.mcutem = e.EMStem_2.value.toString() || "0"
    if (e.PowerFactor_2?.value !== undefined) LineTwo.powerFactor = e.PowerFactor_2.value.toString() || "0"
    if (e.LTV?.value !== undefined) LineTwo.voltage = e.LTV.value.toString() || "0"
    if (e.CO2Emissions_2?.value !== undefined) LineTwo.CO2Emissions = e.CO2Emissions_2.value.toString() || "0"
    if (e.Frequency_2?.value !== undefined) LineTwo.frequency = e.Frequency_2.value.toString() || "0"
    if (e.CRLTsta?.value == 0) LineTwo.Relay = false
    else if(e.CRLTsta?.value == 1) LineTwo.Relay = true
  },
    changeDate(e){
         this.setData({ date:e.detail.value});
   },
    changeTime(e){
     this.setData({ time: e.detail.value });
   },
   changeDateTime(e){
     this.setData({ dateTime: e.detail.value });
     this.changeDateTimeColumn(e);
   },
    changeDateTime1(e) {
     this.setData({ dateTime1: e.detail.value });
   },
    changeDateTimeColumn(e){
     var arr = this.data.dateTime, dateArr = this.data.dateTimeArray;
     arr[e.detail.column] = e.detail.value;
         dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
     
         this.setData({
           dateTimeArray: dateArr,
           dateTime: arr
         });
    },
    changeDateTimeColumn1(e) {
         var arr = this.data.dateTime1, dateArr = this.data.dateTimeArray1;
     
         arr[e.detail.column] = e.detail.value;
         dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
 
     this.setData({ 
       dateTimeArray1: dateArr,
       dateTime1: arr
     });
   }
})
